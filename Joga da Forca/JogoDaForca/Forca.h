#ifndef FORCA_H_
#define FORCA_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>    
#include <time.h>  

using namespace std;

/**
* Modo de Jogo Classico
* @param palavra do jogo 
* @param tema do jogo
**/
void jogoClassico(string palavra, string tema);

/**
* Modo de Jogo Avan�ado
* @param palavra do jogo 
* @param tema do jogo
**/
void jogoAvancado(string palavra, string tema);

/**
* Modo de Jogo Aproximado
* @param palavra do jogo 
* @param tema do jogo
**/
void jogoAproximado(string palavra, string tema);


#endif