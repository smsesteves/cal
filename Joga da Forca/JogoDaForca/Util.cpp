#include "Util.h"

using namespace std;

vector<string> lerPalavrasTema(string tema)
{
	vector<string> retorno;
	string linha;
	ifstream fich_palavras ("palavras.txt");

	bool encontrou = false;

	if (fich_palavras.is_open())
	{
		while ( !fich_palavras.eof() )
		{
			getline (fich_palavras,linha);

			if(!encontrou) {				
				if(linha == "*" + tema)
					encontrou = true;
			} else {
				if(linha == "-")
					break;
				else
					retorno.push_back(linha);
			}
		}
	}

	//retorno.erase(retorno.begin() + retorno.size() - 1);

	fich_palavras.close();
	return retorno;
}


vector<string> lerTemas()
{
	vector<string> retorno;
	string linha;
	ifstream fich_palavras ("palavras.txt");

	bool encontrou = false;

	if (fich_palavras.is_open())
	{
		while ( !fich_palavras.eof() )
		{
			getline (fich_palavras,linha);				
			if(linha[0] == '*')
			{
				linha.erase(0,1);
				retorno.push_back(linha);
			}

		}
	}

	//retorno.erase(retorno.begin() + retorno.size() - 1);

	fich_palavras.close();
	return retorno;
}


vector<int> KMPprefixo(string padrao)
{
	vector<int> prefixo(padrao.length());
    int k = -1;
	prefixo[0] = k;

	for (unsigned int i = 1; i < padrao.length(); i++) 
   {
		while (k > -1 && padrao[k+1] != padrao[i])
			k = prefixo[k];
		if (padrao[i] == padrao[k+1])
			k++;
		prefixo[i] = k;
	}

	return prefixo;
}

vector<int> KMPmacther(string texto, string padrao)
{
	vector<int> ocorrencias;

	int i;
	vector<int> pi =  KMPprefixo(padrao);
	int k = -1;
	
	for (i = 0; i < texto.length(); i++) 
	{
		while (k > -1 && padrao[k+1] != texto[i])
			k = pi[k];
		if (texto[i] == padrao[k+1])
			k++;
		if (k == padrao.length() - 1) {
			cout << "ocorrencia em "<< i-k << endl;
			ocorrencias.push_back(i-k);
		}
	}

	for(int j  = 0; j < ocorrencias.size(); j++)
	{
		cout << ocorrencias[j] << " ";
		
	}
	cout << endl;

	return ocorrencias;
}


int matchDistance(string p, string t) {

	int* D = new int[t.size()];

	for(int j = 0; j < t.size(); j++)
		D[j] = j;

	int old;
	for(int i = 1; i < p.size(); i++) {
		int old = D[0];
		D[0] = i;
		for(int j = 1; j < t.size(); j++)  {
			int newV;
			if(p[i] == t[i])
				newV = old;
			else
				newV = 1 + min(min(old, D[j]), D[j-1]);
			old = D[j];
			D[j] = newV;
		}
	}

	return D[t.size()-1];
}

int matchDistance2(string p, string t) {

	int** D = new int*[p.size()];
	for (int i = 0; i < p.size(); ++i)
		D[i] = new int[t.size()];

	for(int i = 0; i < p.size(); i++) 
		D[i][0] = i;

	for(int i = 0; i < t.size(); i++)
		D[0][i] = i;

	for(int i = 1; i < p.size(); i++)
		for(int j = 1; j < t.size(); j++) {
			if(p[i] == t[j])
				D[i][j] = D[i-1][j-1];
			else
				D[i][j] = 1 + min(min(D[i-1][j-1], D[i-1][j]), D[i][j-1]);
		}

	return D[p.size()-1][t.size()-1];
}

void drawMan(int tentativa) {

	cout << "      _______" << endl;
	cout << "     |/      |" << endl;
	cout << "     |      ";
	if(tentativa >= 1) 
		cout << "(_)";
	cout << endl;

	cout << "     |      ";
	if(tentativa == 2)
		cout << " |";
	else if(tentativa == 3)
		cout << "\\|";
	else if(tentativa >= 4)
		cout << "\\|/";
	cout << endl;

	cout << "     |       ";
	if(tentativa >= 2)
		cout << "|";
	cout << endl;

	cout << "     |       ";
	if(tentativa == 5)
		cout << "/";
	else if(tentativa >= 6)
		cout << "/\\";
	cout << endl;
	cout << "     |" << endl;
	cout << "_____|___" << endl;
}