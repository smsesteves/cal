#ifndef UTIL_H_
#define UTIL_H_

#include <vector>
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

/**
 * Fun��o que procura palavras de determinado tema no dicion�rio
 * @param tema 
 * @return vector de palavras
 **/
vector<string> lerPalavrasTema(string tema);

vector<int> KMPprefixo(string padrao);

/**
 * Algoritmo de pesquisa exacta de substring
 * @param palavra 
 * @param padr�o a pesquisar 
 * @return vector de indexes
 **/
vector<int> KMPmacther(string texto, string padrao);

/**
 * Fun��o que procura temas no dicion�rio
 * @return vector de temas
 **/
vector<string> lerTemas();

/**
 * Algoritmo de pesquisa aproximada - Espaco: O( t.size() )
 * @param palavra @param palavra a comparar @return distancia
 **/
int matchDistance(string p, string t);

/**
 * N�o usado - Algoritmo de pesquisa aproximada - Espaco: O(p.size() * t.size() )
 * @param palavra @param palavra a comparar 
 * @return distancia
 **/
int matchDistance2(string p, string t);

/**
 * Fun��o auxiliar de desenho do HangMan
 * @param numero da tentativa
 **/
void drawMan(int tentativa);

#endif