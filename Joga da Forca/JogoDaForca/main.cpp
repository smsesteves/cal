#include "Util.h"
#include "Forca.h"

using namespace std;

int main()
{
	srand (time(NULL));

	while(true) {
		string input, tema;
		vector<string> palavras;
		vector<string> temas;
		// Escolher o tema
		while(true) {
			system("cls");
			cout << "Jogo da Forca - Escolhe o Tema" << endl << endl;

			cout << "Temas disponiveis: ";

			temas=lerTemas();
			for(int i=0;i<temas.size()-1;i++)
			{
				cout<<temas[i]<<",";
			}
			cout<<temas[temas.size()-1]<<endl;

			cout << "Tema: ";
			getline(cin, tema);

			palavras = lerPalavrasTema(tema);

			if(palavras.size() > 0)
				break;

			cout << "Tema invalido. Tente novamente.";	
			getline(cin, tema);
		} 

		// Escolher o modo
		while(true) {
			system("cls");
			cout << "Jogo da Forca - Escolhe o Modo de Jogo" << endl 
				 << "Tema: " << tema << endl << endl;
			cout << "Modos de jogo:" << endl
				 << "1 - Aproximado" << endl
				 << "2 - Classico" << endl
				 << "3 - Substrings (extra)" << endl << endl;

			cout << "Modo: ";
			getline(cin, input);

			if(input == "1" || input == "2" || input == "3")
				break;

			cout << "Modo invalido. Tente novamente.";
			getline(cin, input);
		}

		string palavra = palavras[rand() % palavras.size()];

		if(input == "1")
			jogoAproximado(palavra, tema);
		else if(input == "2")
			jogoClassico(palavra, tema);
		else if(input == "3")
			jogoAvancado(palavra, tema);
	}

	
	return 0;
}