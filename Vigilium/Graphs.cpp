#include "Graphs.h"

int Vertex::last_num = -1;

Vertex::Vertex() {
	last_num++;
	num = last_num;
	hasGuards = false;
	isFinal = false;
}

void Vertex::addEdge(Edge * edgeAdj) {
	adj.push_back(edgeAdj);
}

void Vertex::addEdgeFlow() {
	for(int i = 0; i < adj.size(); i++)
		adj[i]->flow++;
}

void Vertex::removeEdgeFlow() {
	for(int i = 0; i < adj.size(); i++)
		adj[i]->flow--;
}

int Vertex::getAdjNeededCapacity() {
	int total = 0;
	for(int i = 0; i < adj.size(); i++)
		if(adj[i]->needFlow())
			total++;
	return total;
}



int Edge::last_num = -1;

Edge::Edge(Vertex *o, Vertex *d, double w, double f): orig(o), dest(d), weight(w), flow(f){
	last_num++;
	num = last_num;
}

double Edge::getFlow() const {
	return flow;
}

double Edge::getWeight() const {
	return weight;
}

bool Edge::needFlow() const {
	return (weight > flow);
}

Vertex* Edge::getDest() const {
	return dest;
}

Vertex* Edge::getOrig() const {
	return orig;
}



bool Graph::addVertex(Vertex * in) {

	for (int i = 0; i < vertexSet.size(); i++)
		if (vertexSet[i] == in) return false;

	vertexSet.push_back(in);
	return true;
}

bool Graph::addEdge(Vertex * sourc, Vertex * dest, double w, double f) {

	if(sourc == dest) return false;

	int found=0;

	for (int i = 0; i < vertexSet.size(); i++)
		if (vertexSet[i] == sourc || vertexSet[i] == dest) found++;

	if (found!=2) return false;

	Edge* newEdge = new Edge(sourc, dest, w, f);

	edgeSet.push_back(newEdge);
	sourc->addEdge(newEdge);
	dest->addEdge(newEdge);

	return true;
}

int Graph::getNumVertex() const {
	return vertexSet.size();
}

int Graph::getNumEdges() const {
	return edgeSet.size();
}

int Graph::getNumUnprotectedEdges() const {
	int total = 0;
	for(int i = 0; i < edgeSet.size(); i++)
		if(edgeSet[i]->needFlow())
			total++;

	return total;
}

vector<Vertex * > Graph::getVertexSet() {
	return vertexSet;
}

vector<Edge * > Graph::getEdgeSet() {
	return edgeSet;
}

void Graph::addImportantVigilium() {

	for(int i = 0; i < edgeSet.size(); i++) {
		if(edgeSet[i]->getWeight() == 2) {
			if(!edgeSet[i]->getOrig()->getHasGuards()) {
				edgeSet[i]->getOrig()->setHasGuards(true);
				edgeSet[i]->getOrig()->setIsFinal(true);
				edgeSet[i]->getOrig()->addEdgeFlow();
			}

			if(!edgeSet[i]->getDest()->getHasGuards()) {
				edgeSet[i]->getDest()->setHasGuards(true);
				edgeSet[i]->getDest()->setIsFinal(true);
				edgeSet[i]->getDest()->addEdgeFlow();
			}
		}		
	}

}

vector<vertexValue> Graph::calculateBetterSpots() {
	vector<vertexValue> spots;

	for(int i = 0; i < vertexSet.size(); i++) {
		if(vertexSet[i]->getAdjNeededCapacity() > 0)
			spots.push_back(vertexValue(vertexSet[i]));
	}

	sort(spots.begin(), spots.end());

	return spots;
}
