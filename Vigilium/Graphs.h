#ifndef GRAPHS_H_
#define GRAPHS_H_

#include <vector>
#include <queue>
#include <list>
#include <limits>
#include <cmath>
#include <iostream>
#include <algorithm>
using namespace std;

class Edge;
class Graph;
class vertexValue;

/* ================================================================================================
 * Class Vertex
 * ================================================================================================
 */
class Vertex {
	vector<Edge *> adj;
	int num;
	static int last_num;

	bool hasGuards;
	bool isFinal;
public:

	friend class Graph;
	/**
		Construtor por defeito
	*/
	Vertex();
	/**

		Construtor id
		@param id do Vertex
	*/
	Vertex(int id):num(id) {
		last_num = id +1;
		hasGuards = false;
		isFinal = false;
	}


	/**
		Adiciona aresta adjacente ao vertice
		@param Apontador para a aresta
	*/
	void addEdge(Edge * edgeAdj);

	/**
	
		Retorna todas as arestas adjacente ao vertice
		@return Vetor de Arestas adjacente ao Vertice
	*/
	vector<Edge *> getAdj() { return adj; }

	/**
		Adiciona as arestas a respetiva quantidade de vigia
	*/
	void addEdgeFlow();
	/**
		Adiciona das arestas a respetiva quantidade de vigia
	*/
	void removeEdgeFlow();

	/**
		Retorna o numero de aresta que necessitam de vigia
		@return numero de arestas
	*/
	int getAdjNeededCapacity();

	/**
		Verifica se tem vigia
		@return true ou false
	*/
	bool getHasGuards() const { return hasGuards; }
	/**
		Verifica se a vigia ja e definitiva
		@return true ou false
	*/
	bool getIsFinal() const {return isFinal; }

	/**
		Adiciona vigia
		@param true ou false
	*/
	void setHasGuards(bool HasGuards) { hasGuards = HasGuards; }
	/**
		Adiciona vigia definitiva
		@param true ou false
	*/
	void setIsFinal(bool IsFinal) { IsFinal = isFinal; }

	/**
		Retorna o id do Vertice
		@return Vertex id
	*/
	int getNum(){
		return num;
	}
};


/* ================================================================================================
 * Class Edge
 * ================================================================================================
 */
class Edge {
	Vertex * dest;
	Vertex * orig;
	double weight;
	double flow;
	int num;
	static int last_num;
public:
	/**
	Construtor
	@param Apontador para o vertice Origem
	@param Apontador para o vertice Destino
	@param Quantidade de vigia
	@param Quantidade atual de vigia
	*/
	Edge(Vertex *o, Vertex *d, double w, double f=0);

	/**
	Retorna o Vertice Origem
	@return Apontador para o Vertice Origem
	*/
	Vertex *getOrig() const;
	/**
	Retorna o Vertice Destino
	@return Apontador para o Vertice Destino
	*/
	Vertex *getDest() const;
	/**
	Retorna o numero de vigias atuais
	@return numero de vigias
	*/
	double getFlow() const;
	/**
	Retorna o tota de vigias necessarias
	@return numero total
	*/
	double getWeight() const;
	/**
	Retorna o id da Aresta
	@return aresta id
	*/
	int getNum() const { return num; }

	/**
	Funcao que verifica se a aresta necessita de vigia
	@return true se necessita de vigia ou false no caso contrario
	*/
	bool needFlow() const; 
	/**
	Funcao que incrementa o numero de vigias
	
	*/
	void addFlow() { flow++; }

	friend class Graph;
	friend class Vertex;
};

/* ================================================================================================
 * Class Graph
 * ================================================================================================
 */
class Graph {
	vector<Vertex *> vertexSet;
	vector<Edge *> edgeSet;

public:

	/**
	Adiciona vertice ao grafo
	@param Apontador para o Vertice
	*/
	bool addVertex(Vertex * in);
	/**
	Adiciona Aresta ao grafo
	@param Apontador para o Vertice Origem
	@param Apontador para o Vertice Destino
	@param Apontador para o Vertice Capacidade Necessatio
	@param Apontador para o Vertice Capacidade Atual
	*/
	bool addEdge(Vertex * sourc, Vertex * dest, double w,double f=0);
	/*
	Retorna o numero de vertices do grafo
	@return int num vertives
	*/
	int getNumVertex() const;
	/*
	Retorna o numero de Arestas do grafo
	@return int num arestas
	*/
	int getNumEdges() const;
	/*
	Retorna o numero de Arestas nao protegidas do grafo
	@return int num arestas
	*/
	int getNumUnprotectedEdges() const;
	/*
	Retorna todos os vertices do Grafo
	@return Vetor de apontadores
	*/
	vector<Vertex *> getVertexSet();
	/*
	Retorna todos os arestas do Grafo
	@return Vetor de apontadores
	*/
	vector<Edge *> getEdgeSet();
	/**
	Funcao que coloca vigias nas arestas que necessitam de vigia adicional
	*/
	void addImportantVigilium();
	/*
	Funcao que verifica quais os melhores vertices para colcoar vigias
	@return Vetor de apontadores para os melhores vertices
	*/
	vector<vertexValue> calculateBetterSpots();
};

/* ================================================================================================
 * Class Vertex Value
 * ================================================================================================
 */
class vertexValue {
	int capacity;
	Vertex* vertex;
public:
	/**
	Construtor
	@param Apontador para o Vertice
	*/
	vertexValue(Vertex* vertex) {
		this->vertex = vertex;

		capacity = vertex->getAdjNeededCapacity();
	}
	/**
	Get da variavel capacity
	@return Retorna a capacidade de vigia necessaria
	*/
	int getCapacity() const { return capacity; }
	/**
	Get Vertex
	@return Retorna o Apontador para o Vertice
	*/
	Vertex* getVertex() { return vertex; }
	
	/**
	Atualiza a variavel capacity
	*/
	void update() { capacity = vertex->getAdjNeededCapacity(); }
	/**
	Operador Comparacao
	*/
	bool operator< (const vertexValue &b) const{
		return (capacity > b.getCapacity());
	}
};

#endif
