#include "input.h"

void getInput(Graph* city) {
	int op=0;
	cout<<"PRAFECTUS VIGILUM NA ROMA ANTIGA" << endl << endl
	    <<"1-Introduzir Manualmente"<<endl
	    <<"2-Carregar ficheiro"<<endl;
	do{
		cout<<"Introduza a opcao desejada"<<endl;
		cin>>op;
	} while(op!=1 && op!=2);

	if(op==1)
		readInput(city);
	else {
		string name;
		cout<<"Introduza o nome do ficheiro"<<endl;
		cin>>name;
		filereader(name,city);
	}

}

int readInput(Graph* city) {

	vector<Vertex*> vertex;
	int nvertex,narestas;
	cout<<"Introduza o numero de vertices: "<<endl;
	cin>>nvertex;
	for(int i=0;i<nvertex;i++)
	{
		int id;
		cout<<"Introduza o id"<<endl;
		cin>>id;
		vertex.push_back(new Vertex(id));
		city->addVertex(vertex[vertex.size()-1]);
	}
	cout<<"Introduza o numero de arestas: "<<endl;
	cin>>narestas;
	for(int i=0;i<narestas;i++)
	{
		int n1,n2,n3;
		cout<<"Introduza a aresta (id source, id dest, weight)"<<endl;
		cin>>n1>>n2>>n3;
		//cout << n1 << " " << n2 << " " << n3 << endl;
		int found = 0;
		for(int j=0;j<vertex.size();j++)
		{
			if(vertex[j]->getNum()==n1)
			{

				found++;
				n1=j;
				break;
			}						
		}

		for(int j=0;j<vertex.size();j++)
		{
			if(vertex[j]->getNum()==n2)
			{
				found++;
				n2=j;
				break;
			}						
		} 
		if(found!=2)
		{
			cout<<"Erro"<<endl;
			i--;
			continue;
		}
		city->addEdge(vertex[n1],vertex[n2],n3);
	}
	return 0;
}

int filereader(string path,Graph* city)
{
	vector<Vertex*> vertex;
	bool vertexbool=false;
	bool edgesbool=false;
	string aux;
	ifstream fin(path.c_str());
	if(fin.is_open())
	{
		while(!fin.eof())
		{
			getline (fin,aux);

			if(aux=="Vertex")
			{
				vertexbool=true;
				edgesbool=false;
				continue;
			} else if(aux=="Edges") {
				vertexbool=false;
				edgesbool=true;
				continue;
			}

			if(vertexbool)
			{
				vertex.push_back(new Vertex(atoi(aux.c_str())));
				city->addVertex(vertex[vertex.size()-1]);
			}
			else if(edgesbool)
			{
				int n1=0,n2=0,n3=0;
				stringstream tmp;
				tmp.clear();
				tmp<<aux;
				tmp>>n1;
				tmp>>n2;
				tmp>>n3;
				Vertex* n1v, n2v;
				int found = 0;
				for(int i=0;i<vertex.size();i++)
				{
					if(vertex[i]->getNum()==n1)
					{
						n1 = i;	
						found++;
						break;
					}						
				}

				for(int i=0;i<vertex.size();i++)
				{
					if(vertex[i]->getNum()==n2)
					{
						n2 = i;	
						found++;
						break;
					}						
				} 

				if(found!=2)
					continue;
				city->addEdge(vertex[n1],vertex[n2],n3);
			}
		}
		return 1;
	}
	return -1;
}


