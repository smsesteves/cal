#include <cstdio>
#include <stdlib.h>
#include <windows.h>

#include <iostream>
#include <sstream>

#include <stack>
#include <string>
#include <vector>

#include <algorithm>

#include "Graphs.h"
#include "input.h"

#include "graphviewer.h"

#define GRAPHIC 1

void initWindow(GraphViewer* gv, Graph* city) {
	vector< Vertex* > vertexSet = city->getVertexSet();
	for(int i = 0; i < vertexSet.size(); i++)
		gv->addNode(vertexSet[i]->getNum());

	vector< Edge* > edgeSet = city->getEdgeSet();
	for(int i = 0; i < edgeSet.size(); i++) {
		gv->addEdge(edgeSet[i]->getNum(), edgeSet[i]->getOrig()->getNum(), edgeSet[i]->getDest()->getNum(), EdgeType::UNDIRECTED);
		stringstream ss;
		ss << edgeSet[i]->getWeight();
		gv->setEdgeLabel(edgeSet[i]->getNum(), ss.str());
	}

	gv->rearrange();
}

void recolorWindow(GraphViewer* gv, Graph* city, bool wait = true) {
	if(wait)
		Sleep(700);
	vector< Vertex* > vertexSet = city->getVertexSet();
	for(int i = 0; i < vertexSet.size(); i++) {
		if(vertexSet[i]->getHasGuards())
			gv->setVertexColor(vertexSet[i]->getNum(), "red");
		else
			gv->setVertexColor(vertexSet[i]->getNum(), "lightGray");
	}

	vector< Edge* > edgeSet = city->getEdgeSet();
	for(int i = 0; i < edgeSet.size(); i++)
		if(!edgeSet[i]->needFlow())
			gv->setEdgeColor(edgeSet[i]->getNum(), "red");
		else
			gv->setEdgeColor(edgeSet[i]->getNum(), "lightGray");


	gv->rearrange();
}

int main() {
	Graph* city = new Graph();

	// JANELA
	//GraphViewer *gv = new GraphViewer(600, 600, true, 7772);
	//gv->createWindow(600, 600);
	//gv->defineEdgeColor("lightGray");
	//gv->defineVertexColor("lightGray");

	// INPUT
	getInput(city);

	// Init JANELA
	//initWindow(gv, city);

	// Tempo
	LARGE_INTEGER frequency;
	QueryPerformanceFrequency(&frequency); // (execute once)
	LARGE_INTEGER begin;
	QueryPerformanceCounter(&begin);

	// Inicio dos c�lculos
	city->addImportantVigilium();

	//recolorWindow(gv, city);

	vector<vertexValue> spots = city->calculateBetterSpots();

	for(int i = 1; i <= spots.size(); i++) {
		// TESTE
		//cout << endl << endl << i << endl;

		//recolorWindow(gv, city, false);

		int total = 0;
		for(int j = 0; j < i; j++)
			total += spots[j].getCapacity();

		// TESTE
		//cout << "Total: " << total << " | Unprotected: " << city->getNumUnprotectedEdges() << endl;

		if(city->getNumUnprotectedEdges() > total)
			continue;

		stack< vector<vertexValue> > usingSpotsStack;
		usingSpotsStack.push(spots);
		stack<Vertex *> newGuardedSpots;		

		int pivot = 0, numSpots = 0;
		bool done = false;
		while(!usingSpotsStack.empty()) {
			vector<vertexValue> usingSpots = usingSpotsStack.top();

			if(usingSpots.size() == 0)
				break;

			// Check Overall Total
			int overalltotal = 0;
			for(int j = 0; j < usingSpots.size(); j++)
				overalltotal += usingSpots[j].getCapacity();
			if(overalltotal < city->getNumUnprotectedEdges())
				break;

			// TESTE
			//for(int i = 0; i < usingSpots.size(); i++)
			//	cout << usingSpots[i].getCapacity() << " ";
			//cout << endl;			

			// Adicionar Guarda
			newGuardedSpots.push(usingSpots[0].getVertex());
			usingSpots[0].getVertex()->setHasGuards(true);
			usingSpots[0].getVertex()->addEdgeFlow();

			//recolorWindow(gv, city);

			// Eliminar vertice da lista
			usingSpots.erase(usingSpots.begin());

			// Actualizar novo vector
			for(int k = 0; k < usingSpots.size(); k++)
				usingSpots[k].update();
			sort(usingSpots.begin(), usingSpots.end());

			total = 0;
			for(int j = 0; j < (i-numSpots-1) && j < usingSpots.size(); j++)
				total += usingSpots[j].getCapacity();

			// TESTE
			//for(int i = 0; i < usingSpots.size(); i++)
			//	cout << usingSpots[i].getCapacity() << " ";
			//cout << endl;

			// Verificar se ainda d�, n�o d� ou j� est�
			int numUnprotectedEdges = city->getNumUnprotectedEdges();
			if(numUnprotectedEdges == 0) {
				// Encontrou e acabou
				done = true;
				break;
			} else if(numUnprotectedEdges > total) {
				// N�o d�

				// Eliminar guarda do v�rtice
				Vertex * v = newGuardedSpots.top();
				newGuardedSpots.pop();
				v->setHasGuards(false);
				v->removeEdgeFlow();

				//recolorWindow(gv, city);

				// Overall Total
				overalltotal = 0;
				for(int j = 0; j < usingSpots.size(); j++)
					overalltotal += usingSpots[j].getCapacity();

				// Eliminar v�rtice dos Spots a usar
				usingSpots = usingSpotsStack.top();
				// Se n�o for ultimo vertice a tentar esta combina��o
				if(usingSpots.size() > 1 && overalltotal > city->getNumUnprotectedEdges()) {
					usingSpotsStack.pop();						
				} else {
					while(usingSpotsStack.size() > 1) {
						usingSpotsStack.pop();

						// Eliminar guarda do v�rtice
						if(!newGuardedSpots.empty()) {							
							Vertex * v = newGuardedSpots.top();
							newGuardedSpots.pop();
							v->setHasGuards(false);
							v->removeEdgeFlow();
						}

						//recolorWindow(gv, city);
					}

					usingSpots = usingSpotsStack.top();
					usingSpotsStack.pop();
					numSpots = 0;
				}

				usingSpots.erase(usingSpots.begin());
				usingSpotsStack.push(usingSpots);

			} else {
				// Deu
				// Continua
				usingSpotsStack.push(usingSpots);
				numSpots++;
			}

			//cout << "T: " << total << " | U: " << city->getNumUnprotectedEdges() << "| V: " << newGuardedSpots.size() << endl;
		}



		if(done) {
			cout << "Resultado: " << i << endl;
			break;
		} else {
			while(!newGuardedSpots.empty()) {
				Vertex * v = newGuardedSpots.top();
				newGuardedSpots.pop();
				v->setHasGuards(false);
				v->removeEdgeFlow();
			}
		}
	}


	LARGE_INTEGER end;
	QueryPerformanceCounter(&end);

	LONGLONG elapsed = end.QuadPart - begin.QuadPart;
	LONGLONG nanoseconds = elapsed * 1000000000L / frequency.QuadPart;
	LONGLONG milliseconds = nanoseconds / 1000000L;

	cout << nanoseconds << " nanosegundos = " << milliseconds << " milisegundos" << endl;


	// PRINT
	/*vector< Vertex* > vertexSet = city->getVertexSet();
	for(int i = 0; i < vertexSet.size(); i++) {
		//vector< Edge *> edges = vertexSet[i]->getAdj();
		if(vertexSet[i]->getHasGuards())
			cout << endl << "Vertex " << vertexSet[i]->getNum();
	}*/

	//recolorWindow(gv, city);

	system("pause");

	return 0;
}
